var input = require("readline-sync");


function soalno1() {
  var uangAndi = input.questionInt("Masukkan uang Andi: ");
  var hargaKacamata = [500, 600, 700, 800];
  var hargaBaju = [200, 400, 350];
  var hargaSepatu = [400, 350, 200, 300];
  var hargaBuku = [100, 50, 150];

  var kacamataTerpilih = 0;
  var bajuTerpilih = 0;
  var sepatuTerpilih = 0;
  var bukuTerpilih = 0;
  var uangTerpakai = 0;
  var jumlahItem = 0;

  for (var i = 0; i < hargaKacamata.length; i++) {
    if (uangAndi >= hargaKacamata[i]) {
      if (hargaKacamata[i] <= kacamataTerpilih || kacamataTerpilih === 0) {
        kacamataTerpilih = hargaKacamata[i];
      }
    }
  }
  if (uangAndi >= kacamataTerpilih && kacamataTerpilih !== 0) {
    uangAndi = uangAndi - kacamataTerpilih;
    jumlahItem += 1;
    uangTerpakai += kacamataTerpilih;
  }

  for (var i = 0; i < hargaBaju.length; i++) {
    if (uangAndi >= hargaBaju[i] ) {
      if (hargaBaju[i] < bajuTerpilih || bajuTerpilih === 0) {
        bajuTerpilih = hargaBaju[i];
      }
    }
  }
  if (uangAndi >= bajuTerpilih && bajuTerpilih !== 0) {
    uangAndi = uangAndi - bajuTerpilih;
    jumlahItem += 1;
    uangTerpakai += bajuTerpilih;
  }
  for (var i = 0; i < hargaSepatu.length; i++) {
    if (uangAndi >= hargaSepatu[i]) {
      if (hargaSepatu[i] < sepatuTerpilih || sepatuTerpilih === 0) {
        sepatuTerpilih = hargaSepatu[i];
      }
    }
  }
  if (uangAndi >= sepatuTerpilih && sepatuTerpilih !== 0) {
    uangAndi = uangAndi - sepatuTerpilih;
    jumlahItem += 1;
    uangTerpakai += sepatuTerpilih;
  }
  for (var i = 0; i < hargaBuku.length; i++) {
    if (uangAndi >= hargaBuku[i]) {
      if (uangAndi == hargaBuku[i] || bukuTerpilih === 0) {
        bukuTerpilih = hargaBuku[i];
        break;
      } else if (hargaBuku[i] < bukuTerpilih) {
        bukuTerpilih = hargaBuku[i];
      }
    }
  }
  if (uangAndi >= bukuTerpilih && bukuTerpilih !== 0) {
    uangAndi = uangAndi - bukuTerpilih;
    jumlahItem += 1;
    uangTerpakai += bukuTerpilih;
  }

  console.log("Jumlah uang yang dipakai: ", uangTerpakai);
  console.log("Sisa uang Andi: ", uangAndi);
  console.log(
    `Jumlah item yg bisa dibeli:  ${jumlahItem} (kacamata: ${kacamataTerpilih}, baju: ${bajuTerpilih}, sepatu: ${sepatuTerpilih}, buku: ${bukuTerpilih})`
  );
}

soalno1()

function soalno2() {
  var namaBuku = ["A", "B", "C", "D"]
  var durasiPeminjaman = [14, 3, 7, 7]
  var denda = 100

  console.log("a. 28 Februari - 7 Maret");

  var rentangA = 8
  var dendaRentangA = []

  for(var i = 0; i < durasiPeminjaman.length; i++) {
    if(rentangA > durasiPeminjaman[i]) {
      dendaRentangA[i] = (rentangA - durasiPeminjaman[i])*100
    } else {
      dendaRentangA[i] = 0
    }
    console.log("Denda buku", namaBuku[i], ": Rp. ", dendaRentangA[i]);
  }

  console.log("\nb. 29 April - 30 Mei");
  var rentangB = 31
  var dendaRentangB = []

  for(var i = 0; i < durasiPeminjaman.length; i++) {
    if(rentangB > durasiPeminjaman[i]) {
      dendaRentangB[i] = (rentangB - durasiPeminjaman[i])*100
    } else {
      dendaRentangB[i] = 0
    }
    console.log("Denda buku", namaBuku[i], ": Rp. ", dendaRentangB[i]);
  }
}

//soalno2()

function soalno3() {
  var masuk = input.question("Masuk : ");
  var keluar = input.question("Keluar: ");
  var tanggalMasuk = Number(masuk.substring(0, 2));
  var tanggalKeluar = Number(keluar.substring(0, 2));
  var waktuMasuk = masuk.substring(masuk.length - 8);
  var waktuKeluar = keluar.substring(keluar.length - 8);
  var jamMasuk = Number(waktuMasuk.substring(0, 2));
  var jamKeluar = Number(waktuKeluar.substring(0, 2));
  var hari = 0;
  var jam = 0;

  hari = tanggalKeluar - tanggalMasuk;

  // if(jamKeluar < jamMasuk) {
  //     jamKeluar = jamKeluar + 24
  // }

  jam = jamKeluar - jamMasuk;

  if (hari > 0) {
    jam = jam + 24;
  }

  if (jam <= 8) {
    tarif = jam * 1000;
  } else if (jam > 8 && jam <= 24) {
    tarif = 8000;
  } else if (jam > 24) {
    tarif = 15000 + (jam - 24) * 1000;
  }

  // console.log(masuk)
  // console.log(keluar);
  // console.log(waktuMasuk);
  // console.log(waktuKeluar);
  // console.log(hari);
  // console.log(jamMasuk);
  // console.log(jamKeluar);
  // console.log(jam);
  console.log(tarif);
}

//soalno3()

/*4.	Buatlah fungsi untuk menampilkan n bilangan prima pertama 
(2,3,5,7) */

function soalno4() {
  var n = input.question("Masukkan nilai n: ");
  var hasil = 0;
  var str = "";

  for (var i = 1; i <= n; i++) {
    hasil = 0;
    for (var j = 1; j <= n; j++) {
      if (i % j == 0) { 
        hasil++; 
      }
    }

    if (hasil == 2) {
      str += i + ",";
    } else {
      n++;
    }
    // console.log(m);
  }

  str = str.substring(0, str.length - 1);
  console.log(str);
}

//soalno4()

/* 5.	Buatlah fungsi untuk menampilkan n bilangan Fibonacci pertama
(1,1,2,3) */
function soalno5() {
  var n = input.question("Masukkan nilai n: ");
  var hasil = 1;
  var angka = 1;
  var angkaSebelum = 0;
  var str = "";

  for (var i = 0; i < n; i++) {
    str = str + hasil + ",";
    hasil = angka + angkaSebelum;
    angkaSebelum = angka;
    angka = hasil;
  }

  str = str.substring(0, str.length - 1);
  console.log(str);
}
//soalno5()

/*6.	Tanpa menggunakan fungsi Reverse, buatlah fungsi untuk menentukan apakah sebuah kata adalah palindrome* atau tidak
*Palindrome adalah kata yang jika dibalik tetap sama, contohnya “katak”, “12021”, “malam”
 */
function soalno6() {
  var kata = input.question("Masukkan kata: ");
  var hasil = "";

  for (var i = kata.length - 1; i >= 0; i--) {
    hasil = hasil + kata[i];
  }

  // console.log(hasil)

  if (kata == hasil) {
    console.log("palindrome");
  } else {
    console.log("not palindrome");
  }
}
//soalno6()

/*7.	Tentukan mean, median, dan modus dari deret berikut. Jika ada lebih dari 2 modus, ambil angka yang nilainya paling kecil 
8 7 0 2 7 1 7 6 3 0 7 1 3 4 6 1 6 4 3
*/
function soalno7() {
  var deretBilangan = input.question("Deret Bilangan: ");
  deretBilangan = deretBilangan.split(" ");
  deretBilangan.sort(function (a, b) {
    return a - b;
  });
  console.log(deretBilangan);
  console.log(deretBilangan.length);

  var mean = 0;
  var median = 0;
  var modus = 0;
  var jumlahBilangan = 0;
  var jumlah = 1;
  var banding = 0

  for (var i = 0; i < deretBilangan.length; i++) {
    jumlahBilangan += Number(deretBilangan[i]);
  }

  mean = jumlahBilangan / deretBilangan.length;

  if (deretBilangan.length % 2 != 0) {
    median = Number(deretBilangan[(deretBilangan.length + 1) / 2 - 1]);
  } else {
    //console.log(Number(deretBilangan[deretBilangan.length / 2]));
    //console.log(Number(deretBilangan[deretBilangan.length / 2 + 1]));
    median =
      (Number(deretBilangan[deretBilangan.length / 2 - 1]) +
        Number(deretBilangan[deretBilangan.length / 2 + 1 - 1])) /
      2;
  }

  for (var i = 0; i < deretBilangan.length; i++) {
    if (deretBilangan[i] === deretBilangan[i + 1]) {
      jumlah += 1;
      if (jumlah > banding){
        modus = deretBilangan[i];
        banding = jumlah
      }
    } else {
      jumlah = 1;
    }
  }

  console.log("mean: ", mean);
  console.log("median: ", median);
  console.log("modus: ", modus);
}

//soalno7()

/* 8.	Tentukan nilai minimal dan maksimal dari penjumlahan 4 komponen deret ini
1 2 4 7 8 6 9*/
function soalno8() {
  var n = input.question("n: ");
  n = n.split(" ");
  n = n.sort(function (a, b) {
    return a - b;
  });
  console.log(n);
  var min = 0;
  var max = 0;

  for (var i = 0; i < 4; i++) {
    min = min + Number(n[i]);
  }

  for (var i = n.length - 1; i > n.length - 5; i--) {
    max = max + Number(n[i]);
  }
  console.log(min, max);
}

//soalno8()

/* 9
N = 3 	=> 3 6 9
N = 4	  => 4 8 12 16
N = 5	  => 5 10 15 20 25

*/
function soalno9() {
  var n = input.question("Masukkan nilai n: ");
  var hasil = "";
  var nilai = n;

  for (var i = 1; i <= n; i++) {
    hasil = hasil + nilai + " "; // 3 6 
    nilai = Number(nilai) + Number(n); // 3 + 3, 6 + 3
  }
  console.log(hasil);
}
//soalno9()


/* Susilo Bambang Yudhoyono => S***o B***g Y***o 
    Rani Tiara => R***i T***a*/
function soalno10() {
  var kalimat = input.question("Masukkan kalimat: ");
  var kata = kalimat.split(" ");
  var kataa = "";
  var hasil = "";

  for (var i = 0; i < kata.length; i++) {
    kataa = kata[i].split("");

    for (var j = 0; j < kataa.length; j++) {
      if (j == 0) {
        hasil = hasil + kataa[j];
      } else if (j == kataa.length - 1) {
        hasil = hasil + kataa[j];
      } else if ( j == 1){
        hasil = hasil + "***";
      }
    }
    hasil = hasil + " ";
  }
  hasil = hasil.substring(0, hasil.length - 1);
  console.log(hasil);
}
//soalno10()

/* */
function soalno11() {
  var kata = input.question("Kata: ");
  kata = kata.split("");
  var hasil = "";
  var n = Math.floor(kata.length / 2);
  // console.log(n);

  for (var i = kata.length - 1; i >= 0; i--) {
    for (var j = 0; j < n; j++) {
      hasil += "*"; // ***
    }
    hasil += kata[i]; // ***a
    for (var j = 0; j < n; j++) {
      hasil += "*"; // ***a***
    }
    hasil += "\n";
  }
  console.log(hasil);
}
//soalno11()


/* 12.	Input: 1 2 1 3 4 7 1 1 5 6 1 8
Output: 1 1 1 1 1 2 3 4 5 6 7 8
*selesaikan dengan TIDAK menggunakan fungsi Sort
 */
function soalno12() {
  var array = input.question("Input  : ");
  array = array.split(" ");
  var banding = array[0]; // 1
  var urutan = [];
  var angka = 0;

  for (var i = 0; i < array.length; i++) {
    for (var j = 0; j < array.length; j++) {
      if (banding > array[j]) { 
        angka = j;
        banding = array[j];
      }
    }
    // console.log(array);
    array.splice(angka, 1);
    // console.log("array",array);
    array.length += 1;
    urutan.push(banding);
    banding = array[0];
    angka = 0;
  }

  console.log("Output : ", urutan.toString().replace(/,/g, " "));
}
//soalno12()

function soalno13() {
  var jam = input.questionInt('Masukkan Jam: ')
  var menit = input.questionInt('Masukkan Menit: ')

  if (jam < 0 || jam > 12 || menit < 0 || menit >=60){
    console.log("Input yang anda masukkan salah, silahkan periksa kembali")
  } else {

    // Hitung sudut
    var sudut = Math.abs(30 * jam - 11 / 2 * menit); // 360 - 27,5 = 332,5

    // Pastikan sudut tidak lebih dari 180 derajat
    if (sudut > 180) {

      sudut = (360 - sudut) // 360 - 332,5 = 27,5
    }
  }
  console.log("Sudut saat ini: ", sudut)

}
//soalno13()


function soalno14() {
  var deret = input.question("deret: ");
  var n = input.question("n: ");
  array = deret.split(" ");
  // console.log(array);

  for (var i = 0; i < n; i++) {
    array.push(array[0]); // tambah belakang
    array.shift(); // apus depan
  }
  console.log(array.toString().replace(/,/g, " "));
}

function soalno15() {
  var time = input.question("Masukkan waktu: ");
  var timeString = time.substring(9, 11);
  var hour = time.substring(0, 2);
  var mnt = time.substring(3, 8);
  var hasil = "";

  if (timeString == "PM" && hour == 12) {
    hour = hour;
  } else if (timeString == "PM") {
    hour = Number(hour) + 12;
  } else if (timeString == "AM" && hour == 12) {
    hour = "00";
  } else {
    hour = hour;
  }

  hasil = hour + ":" + mnt;
  console.log(hasil);
}

function soalno16() {
  var orang = input.question("Jumlah orang yang makan: ");
  var orangAlergi = input.question("Jumlah orang yang alergi: ");

  var menu = [
    "1. Tuna Sandwich           42K",
    "2. Spaghetti Carbonara     50K",
    "3. Tea Pitcher             30K",
    "4. Pizza                   70K",
    "5. Salad                   30K",
  ];

  console.log("Menu: ", menu);

  var hargaMenu = [42000, 50000, 30000, 70000, 30000]
  var menuAlergi = input.question("Masukkan menu alergi: ")

  var pajak = 0.1
  var service = 0.05
  var totalHargaTanpaAlergi = 0
  var totalHargaAlergi = 0

  for (var i = 0; i < hargaMenu.length; i++) {
    if (i != menuAlergi - 1) {
        totalHargaTanpaAlergi += hargaMenu[i]
    } else {
        totalHargaAlergi += hargaMenu[i]
    }
  }

  totalHargaTanpaAlergi = totalHargaTanpaAlergi + ((totalHargaTanpaAlergi * pajak) + (totalHargaTanpaAlergi * service))

  totalHargaAlergi = totalHargaAlergi + ((totalHargaAlergi * pajak) + (totalHargaAlergi * service))

  var bayarAlergi = totalHargaTanpaAlergi / orang

  var bayarTanpaAlergi = (totalHargaTanpaAlergi / orang) + (totalHargaAlergi / (orang - orangAlergi))


  console.log("Uang yang dibayar orang tanpa alergi: ", bayarTanpaAlergi);
  console.log("Uang yang dibayar orang alergi: ", bayarAlergi);
}

function soalno17() {
  var rute = input.question("Rute: ");
  rute = rute.split(" ");

  var total = 0;
  var naik = 1;
  var turun = 1;
  var gunung = 0;
  var lembah = 0;
  var str = "";

  for (var i = 0; i < rute.length; i++) {
    if (rute[i] == "N") {
      total += naik;
      if (i == rute.length - 1) {
        str += total;
      } else {
        str += total + " ";
      }
    } else {
      total -= turun;
      if (i == rute.length - 1) {
        str += total;
      } else {
        str += total + " ";
      }
    }
  }

  var barisan = str.split(" ");
  console.log(barisan);
  for (var i = 0; i < barisan.length; i++) {
    if (Number(barisan[i - 1]) > 0 && Number(barisan[i]) == 0) {
      gunung++;
    } else if (Number(barisan[i - 1]) < 0 && Number(barisan[i]) == 0) {
      lembah++;
    }
  }

  console.log("Gunung: " + gunung + " Lembah: " + lembah);
}

function soalno18() {    
    var jam = [9, 13, 15, 17];
    var kalori = [30, 20, 50, 80];
    var start = 18
    var selisih = 0
    var menitOlahraga = 0
    var totalMinum = 0
  
    for (var i = 0; i< jam.length; i++) {
      selisih = start - jam[i]
      menitOlahraga = menitOlahraga + (0.1 * kalori[i] * selisih)
    }
    totalMinum = (Math.floor(menitOlahraga/30) * 100) + 500
    console.log("Total Donna minum:",totalMinum, "cc")
  
}

function soalno19() {
  var kalimat = input.question("Kalimat: ");
  kalimat = kalimat.toLowerCase();
  kalimat = kalimat.replace(/ /g, "");
  kalimat = kalimat.split("");
  kalimat = kalimat.sort();
  var hasil = "";

  for (var i = 0; i < kalimat.length; i++) {
    if (kalimat[i] != kalimat[i + 1]) {
      hasil = hasil + kalimat[i];
    }
  }

  if (hasil.length == 26) {
    console.log("pangram");
  } else {
    console.log("not pangram");
  }
}

function soalno20() {
   // var A = ["G", "G", "G"];
  // var B = ["B", "B", "B"];
  var jarakAwal = input.questionInt("Masukkan jarak awal: ")
  var AJalan = 0;
  var BJalan = jarakAwal;

  var lanjutMain = "y"
  while (lanjutMain==="y"){

    console.log("INPUT HURUF G (GUNTING), K (KERTAS), B (BATU)")
    var A = input.question("Suit A: ")
    var B = input.question("Suit B: ")


    for (var i = 0; i < A.length; i++) {
      if (A[i] === "G" && B[i] === "K") {
        AJalan += 2;
        BJalan += 1;
      } else if (A[i] === "G" && B[i] === "B") {
        AJalan -= 1;
        BJalan -= 2;
      } else if (A[i] === "K" && B[i] === "B") {
        AJalan += 2;
        BJalan += 1;
      } else if (A[i] === "K" && B[i] === "G") {
        AJalan -= 1;
        BJalan -= 2;
      } else if (A[i] === "B" && B[i] === "G") {
        AJalan += 2;
        BJalan += 1;
      } else if (A[i] === "B" && B[i] === "K") {
        AJalan -= 1;
        BJalan -= 2;
      }
    }
    console.log("Jalan A saat ini: ", AJalan)
    console.log("Jalan B saat ini: ", BJalan)
    if (AJalan === BJalan){
      if (AJalan < 0 && BJalan <0){
        console.log("B Menang")
        break;
      } else {
        console.log("A Menang")
        break;
      }
    }
    lanjutMain = input.question("Lanjut main? (y / n)")
  }
}

soalno20()

//soalno1()
//soalno2()
//soalno3()
//soalno4()
//soalno5()
//soalno6()
//soalno7()
//soalno8()
//soalno9()
//soalno10()
//soalno11()
//soalno12()
//soalno13()
//soalno14()
//soalno15()
//soalno16()
//soalno17()
//soalno18()
//soalno19()
//soalno20()
