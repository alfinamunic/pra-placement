import { Modal, Button, Form } from "react-bootstrap";

const FormInput = (props) => {
  const {
    show,
    close,
    handleChange,
    ObatModel,
    saveHandler,
    errors,
    mode,
    editHandler,
    deleteHandler,
  } = props;

  var title, button;

  if (mode === "create") {
    title = <Modal.Title>Create New</Modal.Title>;
    button = (
      <Button variant="primary" onClick={saveHandler}>
        Save Changes
      </Button>
    );
  } else if (mode === "edit") {
    title = <Modal.Title>Edit Data</Modal.Title>;
    button = (
      <Button variant="warning" onClick={editHandler}>
        Update Data
      </Button>
    );
  } else {
    title = <Modal.Title>Delete Data</Modal.Title>;
    button = (
      <Button variant="danger" onClick={deleteHandler}>
        Delete Data
      </Button>
    );
  }

  return (
    <Modal show={show} onHide={close}>
      <Modal.Header closeButton>
        <Modal.Title>{title}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {mode === "delete" ? (
          <Form>
            <Form.Label>Apakah anda yakin ingin menghapus data ini?</Form.Label>
          </Form>
        ) : (
          <Form>
            <Form.Group className="mb-3">
              <Form.Label>Nama Obat</Form.Label>
              <Form.Control
                type="text"
                placeholder=""
                name="nama"
                value={ObatModel.nama}
                onChange={handleChange}
              />
              <Form.Text className="text-muted">{errors.nama}</Form.Text>
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>Segmentasi</Form.Label>
              <Form.Control
                type="text"
                placeholder=""
                name="segmentasi"
                value={ObatModel.segmentasi}
                onChange={handleChange}
              />
              <Form.Text className="text-muted">{errors.segmentasi}</Form.Text>
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>Kategori</Form.Label>
              <Form.Control
                type="text"
                placeholder=""
                name="kategori"
                value={ObatModel.kategori}
                onChange={handleChange}
              />
              <Form.Text className="text-muted">{errors.kategori}</Form.Text>
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>Komposisi</Form.Label>
              <Form.Control
                type="text"
                placeholder=""
                name="komposisi"
                value={ObatModel.komposisi}
                onChange={handleChange}
              />
              <Form.Text className="text-muted">{errors.komposisi}</Form.Text>
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>Harga</Form.Label>
              <Form.Control
                type="text"
                placeholder=""
                name="harga"
                value={ObatModel.harga}
                onChange={handleChange}
              />
              <Form.Text className="text-muted">{errors.harga}</Form.Text>
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>Gambar</Form.Label>
              <Form.Control
                type="text"
                placeholder=""
                name="gambar"
                value={ObatModel.gambar}
                onChange={handleChange}
              />
              <Form.Text className="text-muted">{errors.gambar}</Form.Text>
            </Form.Group>
          </Form>
        )}
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={close}>
          Close
        </Button>
        {button}
      </Modal.Footer>
    </Modal>
  );
};

export default FormInput;
