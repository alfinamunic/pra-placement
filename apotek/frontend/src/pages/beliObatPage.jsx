import React from "react";
import ObatService from "../services/ObatService";
import { Col, Row, Button, Card, Container } from "react-bootstrap";
import { PaymentModal } from "./paymentModal";

export default class BeliObatPage extends React.Component {
  constructor() {
    super();

    this.state = {
      listObat: [],
      listKeranjang: [],
      show: false,
      payMoney: 0,
      modePayment: ""
    };
  }

  handleGetObat = async () => {
    const response = await ObatService.getAllObat();

    if (response.success) {
      this.setState({
        listObat: response.data,
      });
    }
  };

  componentDidMount() {
    this.handleGetObat();
  }

  handleTambahKeranjang = (data, index) => {
    var { listObat } = this.state;

    var insertQty = { ...data, quantity: 1, product: 1 };
    listObat[index] = insertQty;

    this.setState(
      {
        listObat: listObat
      }
    );
  };

  handleTambahQuantity = (index) => {
    const { listObat } = this.state;
    const quantitys = listObat[index].quantity;
    const addQuantity = quantitys + 1;

    var updateData = [...this.state.listObat];
    updateData[index] = { ...updateData[index], quantity: addQuantity };

    this.setState({
      listObat: updateData,
    });

    console.log("index", quantitys);
  };

  handleKurangQuantity = (index) => {
    const { listObat } = this.state;
    const quantitys = listObat[index].quantity;
    const products = listObat[index].product;

    const addQuantity = quantitys - 1;

    if (addQuantity == 0) {
      var addProducts = 0;
    } else if (quantitys > 0) {
      addProducts = products;
    }

    var updateData = [...this.state.listObat];
    updateData[index] = {
      ...updateData[index],
      quantity: addQuantity,
      product: addProducts,
    };

    console.log("prdct", products);

    this.setState({
      listObat: updateData,
    });
  };

  handleTambahProduct = (item) => {
    if (item.product == undefined) {
      item.product = 0;
    }

    return Number(item.product);
  };

  grandTotalProduct = () => {
    var { listObat } = this.state;

    var grandTotalProduct = listObat.reduce(
      (total, item) => total + Number(this.handleTambahProduct(item)),
      0
    );
    return grandTotalProduct;
  };

  calculateTotal = (item) => {
    // console.log("q", item.quantity);

    if (item.quantity == undefined) {
      item.quantity = 0;
    }
    return Number(item.harga) * Number(item.quantity);
  };

  grandTotal = () => {
    var { listObat } = this.state;

    var grandTotal = listObat.reduce(
      (total, item) => total + Number(this.calculateTotal(item)),
      0
    );

    return grandTotal;
  };

  openPaymentModal = () => {
    this.setState({
      show: true,
    });
  };

  handleChangeMoney = (event) => {
    this.setState({
        payMoney: Number(event.target.value),
      });
  }

  closePaymentModal = () => {
    const {modePayment} = this.state
    this.setState({
        show: false,
        modePayment: "belum bayar"
    },
    () => this.handleGetObat())
  }

  orderPayments = () => {
    const { payMoney } = this.state

    if (payMoney === 0) {
        alert("masukkan uang anda");
      } else if (payMoney < this.grandTotal()) {
        alert("uang anda kurang");
      } else {
          this.setState({
            modePayment: "bayar",
          });
        }
      
  }

  render() {
    const { listObat, show, payMoney, modePayment } = this.state;

    var footer;

    if (this.grandTotalProduct() != 0 && modePayment != "bayar") {
      footer = (
        <>
          <nav
            class="navbar sticky-bottom bg-body-tertiary"
            style={{ height: "80px" }}
          >
            <Container>
              <a class="navbar-brand" href="#">
                {this.grandTotalProduct()} Produk | Estimasi Harga : Rp.{" "}
                {this.grandTotal()}
              </a>
              <Button onClick={this.openPaymentModal}>
                <i class="bi bi-cart2" ></i> Pembayaran
              </Button>
            </Container>
          </nav>
        </>
      );
    } else {
      footer = <></>;
    }

    return (
      <>
        <div>
          <Container style={{ marginTop: "24px"}}>
          <h3>Beli Obat</h3>
            <Row style={{ marginTop: "40px" }}>
              {listObat &&
                listObat.map((data, index) => (
                  <Col sm={6}>
                    <Card style={{marginBottom: "40px"}}>
                      <Card.Body>
                        <Row style={{ height: "120px" }}>
                          <Col>
                            <Card.Title>{data.nama}</Card.Title>
                            <Card.Text
                              style={{
                                fontSize: "16px",
                                marginTop: "8px",
                                marginLeft: "8px"
                              }}
                            >
                              {data.komposisi}
                            </Card.Text>
                            <Card.Text
                              style={{
                                fontSize: "18px",
                                marginTop: "12px",
                                marginLeft: "12px",
                                fontWeight: "bold"
                              }}
                            >
                              Rp. {data.harga}
                            </Card.Text>
                          </Col>
                          <Col sm={3}>
                      <img
                        src={data.gambar}
                        alt="..."
                        style={{ width: "120px", height: "120px" }}
                      />
                    </Col>
                        </Row>
                        <Row
                          style={{
                            marginTop: "12px",
                            textAlign: "center",
                          }}
                        >
                          <>
                            {data.quantity ? (
                              <>
                                <Col
                                  style={{
                                    justifyContent: "center",
                                    marginLeft: "120px",
                                  }}
                                >
                                  <Button
                                    style={{ color: "navy" }}
                                    variant="light"
                                    onClick={() =>
                                      this.handleKurangQuantity(index)
                                    }
                                  >
                                    {" "}
                                    -<i class="bi bi-dash"></i>{" "}
                                  </Button>
                                </Col>
                                <Col sm={1}>
                                  <Card.Text style={{ marginTop: "6px" }}>
                                    {data.quantity}
                                  </Card.Text>
                                </Col>
                                <Col>
                                  <Button
                                    variant="light"
                                    style={{
                                      color: "navy",
                                      marginRight: "120px",
                                    }}
                                    onClick={() =>
                                      this.handleTambahQuantity(index)
                                    }
                                  >
                                    {" "}
                                    +<i class="bi bi-plus"></i>
                                  </Button>
                                </Col>
                              </>
                            ) : (
                              <Col style={{ justifyContent: "center" }}>
                                <Button
                                  style={{ width: "360px" }}
                                  onClick={() =>
                                    this.handleTambahKeranjang(data, index)
                                  }
                                >
                                  Tambah ke Keranjang
                                </Button>
                              </Col>
                            )}
                          </>

                          <Col style={{ textAlign: "center" }} sm={3}>
                          </Col>
                        </Row>
                      </Card.Body>
                    </Card>
                  </Col>
                ))}
            </Row>
          </Container>
        </div>
        {footer}
        <PaymentModal
        show={show}
        grandTotal={this.grandTotal}
        payMoney={payMoney}
        close={this.closePaymentModal}
        handleChangeMoney={this.handleChangeMoney}
        modePayment={modePayment}
        orderPayments={this.orderPayments}/>
      </>
    );
  }
}
