import React from "react";
import ObatService from "../services/ObatService";
import { Container, Button, Row, Col, Form } from "react-bootstrap";
import FormInput from "./FormInput";

export default class ObatPage extends React.Component {
  ObatModel = {
    nama: "",
    segmentasi: "",
    kategori: "",
    komposisi: "",
    harga: "",
    gambar: "",
  };

  params = {
    nama: "",
  };
  constructor() {
    super();

    this.state = {
      listObat: [],
      show: false,
      ObatModel: this.ObatModel,
      errors: {},
      mode: "",
      params: this.params,
    };
  }

  handleGetObat = async () => {
    const response = await ObatService.getAllObat();

    if (response.success) {
      this.setState({
        listObat: response.data,
      });
    }
  };

  componentDidMount() {
    this.handleGetObat();
  }

  handleOpenModal = () => {
    this.setState({
      show: true,
      mode: "create",
    });
  };

  handleCloseModal = () => {
    this.setState({
      show: false,
    });
  };

  handleChange = (event) => {
    const { name, value } = event.target;
    this.setState({
      ObatModel: {
        ...this.state.ObatModel,
        [name]: value,
      },
    });
  };

  saveHandler = async () => {
    const { ObatModel } = this.state;

    if (this.validationHandler()) {
      const response = await ObatService.addDataObat(ObatModel);

      if (response.success) {
        this.handleGetObat();
        this.setState({
          show: false,
          ObatModel: this.ObatModel,
          mode: "create",
        });
      }
    }
  };

  validationHandler = () => {
    const { ObatModel } = this.state;

    var formIsValid = true;
    var errors = {};

    if (ObatModel.nama === "") {
      formIsValid = false;
      errors.nama = "nama obat tidak boleh kosong";
    }

    if (ObatModel.segmentasi === "") {
      formIsValid = false;
      errors.segmentasi = "segmentasi obat tidak boleh kosong";
    }

    if (ObatModel.kategori === "") {
      formIsValid = false;
      errors.kategori = "kategori obat tidak boleh kosong";
    }

    if (ObatModel.komposisi === "") {
      formIsValid = false;
      errors.komposisi = "komposisi obat tidak boleh kosong";
    }

    if (ObatModel.harga === "") {
      formIsValid = false;
      errors.harga = "harga obat tidak boleh kosong";
    }

    this.setState({
      errors: errors,
    });

    return formIsValid;
  };

  openModalEdit = async (id) => {
    const response = await ObatService.getObatById(id);

    if (response.success) {
      this.setState({
        show: true,
        mode: "edit",
        ObatModel: response.data,
      });
    }
  };

  editHandler = async () => {
    const { ObatModel } = this.state;

    if (this.validationHandler()) {
      const response = await ObatService.editDataObat(ObatModel);

      if (response.success) {
        this.handleGetObat();
        this.setState({
          show: false,
          ObatModel: this.ObatModel,
        });
      }
    }
  };

  openModalDelete = async (id) => {
    const response = await ObatService.getObatById(id);

    if (response.success) {
      this.setState({
        show: true,
        mode: "delete",
        ObatModel: response.data,
      });
    }
  };

  deleteHandler = async () => {
    const { ObatModel } = this.state;

    const response = await ObatService.deleteDataObat(ObatModel);

    if (response.success) {
      this.handleGetObat();
      this.setState({
        show: false,
        ObatModel: this.ObatModel,
      });
    }
  };

  handleGetSearch = async () => {
    const { params } = this.state;
    const response = await ObatService.searchObat(params);
    console.log("params", params);

    if (response.success) {
      this.setState({
        listObat: response.data,
      });
    }
  };

  handleChangeSearch = (event) => {
    console.log("Change Search", event);
    const { value } = event.target;
    this.setState(
      {
        params: {
          ...this.state.params,
          nama: value,
        },
      },
      () => this.handleGetSearch()
    );
  };

  render() {
    const { listObat, show, ObatModel, errors, mode } = this.state;
    console.log(listObat);
    console.log(ObatModel);
    return (
      <>
        <Container style={{ marginTop: "24px" }}>
          <div>
            <h3>Data Obat</h3>
            <Row style={{ marginTop: "12px" }}>
              <Col sm={3}>
                <Form.Control
                  type="text"
                  placeholder="search..."
                  onChange={this.handleChangeSearch}
                />
              </Col>
              <Col></Col>
              <Col sm={2}>
                <Button onClick={this.handleOpenModal}>Create New</Button>
              </Col>
            </Row>
          </div>
          <div>
            {listObat.length != 0 ? (
              <>
                <table
                  className="table table-striped table-bordered"
                  style={{ marginTop: "40px" }}
                >
                  <thead>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Segmentasi</th>
                    <th>Kategori</th>
                    <th>Komposisi</th>
                    <th>Harga</th>
                    <th>Gambar</th>
                    <th>Action</th>
                  </thead>
                  <tbody>
                    {listObat.map((data, index) => (
                      <tr key={index}>
                        <td>{index + 1}</td>
                        <td>{data.nama}</td>
                        <td>{data.segmentasi}</td>
                        <td>{data.kategori}</td>
                        <td>{data.komposisi}</td>
                        <td>Rp.{data.harga}</td>
                        <td>
                          <img
                            src={data.gambar}
                            alt="..."
                            style={{ width: "120px" }}
                          />
                        </td>
                        <td>
                          <Button onClick={() => this.openModalEdit(data.id)}>
                            Edit
                          </Button>{" "}
                          <Button onClick={() => this.openModalDelete(data.id)}>
                            Delete
                          </Button>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </>
            ) : (
              <>
                <h6 style={{ marginTop: "120px", marginLeft: "480px" }}>
                  Data tidak ditemukan
                </h6>
              </>
            )}
          </div>
        </Container>
        <FormInput
          show={show}
          close={this.handleCloseModal}
          handleChange={this.handleChange}
          ObatModel={ObatModel}
          saveHandler={this.saveHandler}
          errors={errors}
          mode={mode}
          editHandler={this.editHandler}
          deleteHandler={this.deleteHandler}
        />
      </>
    );
  }
}
