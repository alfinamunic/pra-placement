import React from 'react';
import {BrowserRouter, Route, Routes} from 'react-router-dom'
import ObatPage from './pages/ObatPage';
import "bootstrap/dist/css/bootstrap.css"
import BeliObatPage from './pages/beliObatPage';

export default class App extends React.Component {
  constructor(){
    super()

    this.state = {}
  }
  render() {
  return (
    <>
    <BrowserRouter>
      <Routes>
        <Route path="/obat" element={<ObatPage/>}></Route>
        <Route path='/beli-obat' element={<BeliObatPage/>}></Route>
      </Routes>

    </BrowserRouter>
    </>
  );
}
}

