import axios from "axios";
import { config } from "../config/config";

const ObatService = {
  getAllObat: () => {
    const result = axios
      .get(config.apiUrl + `api/obat`)
      .then((response) => {
        return {
          success: response.data.success,
          data: response.data.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          data: error,
        };
      });
    return result;
  },
  addDataObat: (data) => {
    const result = axios
      .post(config.apiUrl + `api/addobat`, data)
      .then((response) => {
        return {
          success: response.data.success,
          data: response.data.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          data: error,
        };
      });
    return result;
  },
  getObatById: (id) => {
    const result = axios
      .get(config.apiUrl + `api/obat/${id}`)
      .then((response) => {
        return {
          success: response.data.success,
          data: response.data.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          data: error,
        };
      });
    return result;
  },
  editDataObat: (data) => {
    const result = axios
      .put(config.apiUrl + `api/updateobat/${data.id}`, data)
      .then((response) => {
        return {
          success: response.data.success,
          data: response.data.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          data: error,
        };
      });
    return result;
  },
  deleteDataObat: (data) => {
    const result = axios
    .put(config.apiUrl + `api/deleteobat/${data.id}`, data)
    .then((response) => {
        return {
          success: response.data.success,
          data: response.data.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          data: error,
        };
      });
    return result;
  },
  searchObat: (data) => {
    const result = axios
        .get(
            config.apiUrl +
            `api/search/obat?nama=${data.nama}`
        )
        .then((response) => {
            return {
                success: response.data.success,
                data: response.data.data,
            };
        })
        .catch((error) => {
            return {
                success: false,
                data: error,
            };
        });
    return result
},
};

export default ObatService;
