const { request, response } = require("express")
const { errorMonitor } = require("nodemailer/lib/xoauth2")

module.exports = exports = (app, pool) => {
    app.get("/api/obat", (request, response) => {
        const query = 'select * from obat where is_delete = false'

        pool.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    data: error
                })
            } else {
                response.send(200, {
                    success: true,
                    data: result.rows
                })
            }
        })
    })
    app.get("/api/obat/:id", (request, response) => {
        const { id } = request.params
        const query = `SELECT * FROM obat WHERE id=${id}`

        pool.query(query, (error, result) => {
            if(error) {
                response.send(400, {
                    success: false,
                    data: error
                })
            } else {
                response.send(200, {
                    success: true,
                    data: result.rows[0]
                })
            }
        })
    })
    app.post("/api/addobat", (request, response) => {
        const { nama, segmentasi, kategori, komposisi, harga, gambar } = request.body

        const duplicateCheckQuery = `SELECT COUNT(*) as count from obat where nama = '${nama}'`

        pool.query(duplicateCheckQuery, (error, result) => {
            console.log(result)
            if (error) {
                return response.send(500, {
                    success: false,
                    error: "An error occured"
                })
            }

            const duplicateCount = result.rows[0].count 
            if (duplicateCount > 0) {
                return response.send(200, {
                    success: false,
                    data: "Data already exist"
                })
            }

            const query = `INSERT INTO obat(nama, segmentasi, kategori, komposisi, harga, gambar, created_by, created_on) VALUES ('${nama}', '${segmentasi}', '${kategori}', '${komposisi}', ${harga}, '${gambar}', '1', now())`

            pool.query(query, (error, result) => {
                if(error) {
                    return response.send(500, {
                        success: false,
                        data: "an error occured."
                    })
                } else {
                    return response.send(201, {
                        success: true,
                        data: `Data obat ${nama} saved`
                    })
                }
            })
        })
    }) 
    app.put("/api/updateobat/:id", (request, response) => {
        const { nama, segmentasi, kategori, komposisi, harga, gambar } = request.body 

        const { id } = request.params

        const duplicateCheckQuery = `SELECT COUNT(*) as count from obat where nama = '${nama}' AND id!=${id}`

        pool.query(duplicateCheckQuery, (error, result) => {
            if(error) {
                return response.send(500, {
                    success: false,
                    error: "An error occured"
                })
            }

            const duplicateCount = Number(result.rows[0].count)
            if(duplicateCount > 0) {
                return response.send(400, {
                    success: false,
                    data: "Data already exist"
                })
            }

            const query = `UPDATE obat SET nama = '${nama}', segmentasi = '${segmentasi}', kategori = '${kategori}', komposisi = '${komposisi}', harga = '${harga}', gambar = '${gambar}', modified_by = 1, modified_on = now() WHERE id=${id}`

            pool.query(query, (error, result) => {
                if (error) {
                    return response.send(500, {
                        success: false,
                        data: error
                    })
                } else {
                    return response.send(200, {
                        success: true,
                        data: "Data updated"
                    })
                }
            })
        })
    })  
    app.put("/api/deleteobat/:id", (request, response) => {
        const { id } = request.params

        const query = `UPDATE obat SET is_delete = true, deleted_by = 1, deleted_on = now() WHERE id=${id}`

        pool.query(query, (error, result) => {
            if (error) {
                return response.send(400, {
                    success: false,
                    data: error
                })
            } else {
                return response.send(200, {
                    success: true,
                    data: 'Data has been delete'
                })
            }
        })
    })
    app.get("/api/search/obat", (request, response) => {
        var { nama } = request.query

        const queryAll = `SELECT * FROM obat where is_delete = false`

        const query = `SELECT * FROM obat where nama ilike ('%${nama.trim()}%') and is_delete = false order by nama`

        pool.query(queryAll, (error, result) => {
            pool.query(query, (errors, results) => {
                if (errors) {
                    return response.send(400, {
                        success: false,
                        data: errors
                    })
                } else {
                    return response.send(200, {
                        success: true,
                        data: results.rows
                    })
                }
            })
        })
    })
}