var express = require("express")
var dotenv = require('dotenv')
var app = express()
dotenv.config()

//file index manjadi main 

var bodyParser = require('body-parser')
var cors = require('cors')

app.use(cors())
app.use(bodyParser.json())
app.use(
    bodyParser.urlencoded({
        extended: true,
    })
)

global.config = require('./config/dbconfig')

// localhost:8000/
app.get("/", (req, res) => {
    res.send("Hello, World!")
})

// app.get("/students", (req, res) => {
//     res.send({
//         students: [
//             {id: 1, names: "John"},
//             {id: 2, names: "Jane"}
//         ]
//     })
// })

// app.get("/revenue", (req, res) => {
//     res.send({cost: 10000})
// })

require('./services/obatService')(app, global.config.pool)

app.listen(process.env.PORT, () => {
    console.log(`Example app listening on port ${process.env.PORT}`)
}) 