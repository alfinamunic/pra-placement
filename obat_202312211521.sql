create table public.obat (
id bigint primary key generated always as identity not null,
nama varchar(50),
segmentasi varchar(50),
kategori varchar(50),
komposisi text,
harga bigint,
gambar varchar,
created_by bigint not null,
created_on timestamp not null,
modified_by bigint,
modified_on timestamp,
deleted_by bigint,
deleted_on timestamp,
is_delete boolean default false not null)

INSERT INTO public.obat (nama,segmentasi,kategori,komposisi,harga,gambar,created_by,created_on,modified_by,modified_on,deleted_by,deleted_on,is_delete) VALUES
	 ('Panadol Hijau','Obat Bebas','Flu & Batuk','Paracetamol 500mg',12500,'https://www.konimex.com/0_repository/images/20220110032234Paramex-FB-Pe-3.png',1,'2023-12-18 11:10:06.845121',1,'2023-12-19 10:57:26.147963',1,'2023-12-19 11:03:04.435701',false),
	 ('Panadol Biru','Obat Bebas','Sakit Kepala','Paracetamol 500mg',12500,'https://smb-padiumkm-images-public-prod.oss-ap-southeast-5.aliyuncs.com/product/image/08112023/6486c386f9d8ae04ae9df594/654af10cad7e579a4bc91a40/d6db315e5307ea922064e74dfc9d89.jpeg',1,'2023-12-19 08:54:22.621529',1,'2023-12-21 10:33:41.326864',NULL,NULL,false),
	 ('Promag','Obat Bebas','Lambung','Hydrotalcite 200 mg, Mg(OH)2 150 mg, Simethicone 50 mg',10800,'https://images.k24klik.com/product/large/apotek_online_k24klik_2021101902504923085_Promag-Tablet-10s-1.jpg',1,'2023-12-19 13:24:56.4446',1,'2023-12-21 10:34:25.304087',NULL,NULL,false),
	 ('Tolak Angin','Herbal','Masuk Angin','Herbal dan Madu',22000,'https://www.sidomuncul.co.id/assets/images/product/produk-tolak-angin.png',1,'2023-12-21 10:39:17.358467',NULL,NULL,NULL,NULL,false),
	 ('Panadol Extra','Obat Bebas(Hijau)','Sakit Kepala Berat','Paracetamol 500mg dan Caffeine 65mg',14500,'https://www.static-src.com/wcsstore/Indraprastha/images/catalog/full//109/MTA-1995958/panadol_panadol-extra-with-optizorb---20-caplets_full02.jpg',1,'2023-12-19 09:45:00.525591',1,'2023-12-21 10:57:33.613223',1,'2023-12-19 11:14:17.467199',false);
